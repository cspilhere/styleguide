var gulp = require('gulp');
var del = require('del');

gulp.task('clean', function (cb) {
  return del(['.tmp'], cb);
});


// Build
gulp.task('clean:build', function (cb) {
  return del(['build'], cb);
});
