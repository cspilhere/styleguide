var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');


// Build assets
gulp.task('autoprefixer:build', function () {
	return gulp.src('./build/lib/dist/styles/styles.css')
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(gulp.dest('./build/lib/dist/styles'));
});
