var gulp = require('gulp');


// Dev
gulp.task('copy', function () {
	gulp.src(['dev/templates/**/*'])
    .pipe(gulp.dest('.tmp/templates/'));
});


// Build SG
gulp.task('copy:sg', function () {
  gulp.src('dev/fonts/**/*')
    .pipe(gulp.dest('./build/fonts'));

  gulp.src('dev/media/**/*')
    .pipe(gulp.dest('./build/media'));

  gulp.src('dev/index.html')
    .pipe(gulp.dest('./build/'));
});


// Build assets
gulp.task('copy:build:dev', function () {
  gulp.src('dev/styles/**/*')
    .pipe(gulp.dest('./build/lib/dev/styles'));

  gulp.src('dev/scripts/**/*')
    .pipe(gulp.dest('./build/lib/dev/scripts'));

  gulp.src('dev/fonts/**/*')
    .pipe(gulp.dest('./build/lib/dev/fonts'));

  gulp.src('dev/media/**/*')
    .pipe(gulp.dest('./build/lib/dev/media'));
});

gulp.task('copy:build:dist', function () {
  gulp.src('dev/fonts/**/*')
    .pipe(gulp.dest('./build/lib/dist/fonts'));

  gulp.src('dev/media/**/*')
    .pipe(gulp.dest('./build/lib/dist/media'));
});
