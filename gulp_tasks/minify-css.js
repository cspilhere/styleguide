var gulp = require('gulp');
var minifyCss = require('gulp-minify-css');
var sourcemaps = require('gulp-sourcemaps');
var rename = require('gulp-rename');


// Build SG
gulp.task('minify-css:sg', function () {
  return gulp.src('./build/styles/main.css')
    .pipe(sourcemaps.init())
    .pipe(minifyCss())
    .pipe(rename('main.min.css'))
    .pipe(sourcemaps.write('/'))
    .pipe(gulp.dest('./build/styles'));
});


// Build assets
gulp.task('minify-css:build', function () {
  return gulp.src('./build/lib/dist/styles/styles.css')
    .pipe(sourcemaps.init())
    .pipe(minifyCss())
    .pipe(rename('styles.min.css'))
    .pipe(sourcemaps.write('/'))
    .pipe(gulp.dest('./build/lib/dist/styles'));
});
