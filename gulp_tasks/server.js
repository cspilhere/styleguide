var gulp = require('gulp');
var opn = require('opn');
var connect = require('gulp-connect');

gulp.task('webserver', function() {
  connect.server({
    root: ['./.tmp', './dev'],
    livereload: true,
    middleware: function(connect, opt) {
      return [];
    }
  });
  opn('http://localhost:8080');
});
