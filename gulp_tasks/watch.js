var gulp = require('gulp');
var watch = require('gulp-watch');

gulp.task('webpackReload', function() { gulp.src(['./.tmp/scripts/bundle.js']).pipe(connect.reload()); });

gulp.task('watch', function () {
  gulp.watch(['dev/*.html'], ['html']);
  gulp.watch(['./.tmp/scripts/bundle.js'], ['webpackReload']);
  gulp.watch(['dev/styles/**/*.scss'], ['sass']);
});
