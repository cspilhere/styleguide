var gulp = require('gulp');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var rename = require('gulp-rename');


// Build SG
gulp.task('uglify:sg', function() {
  gulp.src('./build/scripts/*.js')
    .pipe(sourcemaps.init())
    .pipe(uglify({
      output: {
        space_colon: false,
        comments: false
      },
      mangle: true,
    	compress: {
    		sequences: true,
    		dead_code: true,
    		conditionals: true,
    		booleans: true,
    		unused: true,
    		if_return: true,
    		join_vars: true,
    		drop_console: true
    	},
    }))
    .pipe(sourcemaps.write('/'))
    .pipe(gulp.dest('./build/scripts'));
});


// Build assets
gulp.task('uglify:build:dist', function() {
  gulp.src('./build/lib/dist/scripts/*.js')
    .pipe(sourcemaps.init())
    .pipe(uglify({
      output: {
        space_colon: false,
        comments: false
      },
      mangle: true,
    	compress: {
    		sequences: true,
    		dead_code: true,
    		conditionals: true,
    		booleans: true,
    		unused: true,
    		if_return: true,
    		join_vars: true,
    		drop_console: true
    	},
    }))
    .pipe(rename('bundle.min.js'))
    .pipe(sourcemaps.write('/'))
    .pipe(gulp.dest('./build/lib/dist/scripts'));
});
