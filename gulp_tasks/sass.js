var gulp = require('gulp');
var connect = require('gulp-connect');
var sass = require('gulp-sass');
var rename = require('gulp-rename');


// Dev
gulp.task('sass', function () {
  return gulp.src('./dev/styles/**/*.scss')
    .pipe(sass({ outputStyle: 'expanded'}).on('error', sass.logError))
    .pipe(gulp.dest('./.tmp/styles'))
    .pipe(connect.reload());
});


// Build SG
gulp.task('sass:sg', function () {
  return gulp.src('./dev/styles/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./build/styles'));
});


// Build assets
gulp.task('sass:build', function () {
  return gulp.src('./dev/styles/main.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(rename('styles.css'))
    .pipe(gulp.dest('./build/lib/dist/styles'));
});
