var gulp = require('gulp');
var connect = require('gulp-connect');
var htmlreplace = require('gulp-html-replace');

// Dev
gulp.task('html', function () {
  return gulp.src('./dev/*.html')
    .pipe(connect.reload());
});


// Build SG
gulp.task('htmlReplace:sg', function() {
  gulp.src('./dev/index.html')
    .pipe(htmlreplace({
        'css': './styles/main.min.css'
    }))
    .pipe(gulp.dest('./build'));
});
