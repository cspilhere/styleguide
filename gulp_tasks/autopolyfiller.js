var gulp = require('gulp');
var autopolyfiller = require('gulp-autopolyfiller');


// Build assets
gulp.task('autopolyfiller', function () {
  return gulp.src('./lib/**/*.js')
    .pipe(autopolyfiller('result_polyfill_file.js'))
    .pipe(gulp.dest('./dist'));
});
