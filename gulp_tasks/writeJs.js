var gulp = require('gulp');

var insert = require('gulp-insert');
var file = require('gulp-file');

// BUILD: writeEntryJsFile
gulp.task('writeEntryJsFile', function () {
  var entry = '// JS Modules\n\n';
  entry += 'console.log(\'o\/\');\n\n';
  var str = gulp.src(['./dev/scripts/modules/*.js']).pipe(insert.transform(function(contents, file) {
    var entryModule = '//require(\'./modules/' + file.path.split(file.base)[1].split('.js')[0] + '\')();\n';
    entry += entryModule;
    require('fs').writeFileSync('dist/entry.js', entry);
    return entryModule;
  }));
});
