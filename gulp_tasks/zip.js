var gulp = require('gulp');
var zip = require('gulp-zip');


// Build assets
gulp.task('zip:build', function () {
  setTimeout(function() {
    return gulp.src('./build/lib/**/*')
      .pipe(zip('lib.zip'))
      .pipe(gulp.dest('./build/'));
  }, 10000);
});
