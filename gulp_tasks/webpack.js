var gulp = require('gulp');
var opn = require('opn');
var gutil = require('gulp-util');
var connect = require('gulp-connect');

var webpack = require('webpack');
var webpackStream = require('webpack-stream');

var webpackConfig = require('../webpack.config.js');
var webpackSgConfig = require('../webpack.sg.config.js');
var webpackBuildConfig = require('../webpack.build.config.js');


// Dev
gulp.task('webpack', function(callback) {
  return gulp.src('./dev/_app/core/entry.js')
    .pipe(webpackStream(webpackConfig, webpack, function(err, stats) {}))
    .pipe(gulp.dest('./.tmp/'))
    .pipe(connect.reload());
});


// Build SG
gulp.task('webpack:sg', function(callback) {
  var devCompiler = webpack(Object.create(webpackSgConfig));
	devCompiler.run(function(err, stats) {
		if(err) throw new gutil.PluginError('webpack:sg', err);
		gutil.log('[webpack:sg]', stats.toString({
			colors: true
		}));
		callback();
	});
});


// Build assets
gulp.task('webpack:build', function(callback) {
  var devCompiler = webpack(Object.create(webpackBuildConfig));
	devCompiler.run(function(err, stats) {
		if(err) throw new gutil.PluginError('webpack:build', err);
		gutil.log('[webpack:build]', stats.toString({
			colors: true
		}));
		callback();
	});
});
