var gulp = require('gulp');
var run = require('run-sequence');
require('require-dir')('./gulp_tasks');

var pkg = require('./package.json');


// Main
gulp.task('default', function() {
  run('clean', 'webserver', 'watch', 'sass', 'webpack');
});


// Build
gulp.task('build', function() {
  run(
    'clean:build',
    'sass:sg',
    'sass:build',
    'autoprefixer:build',
    'minify-css:sg',
    'minify-css:build',
    'copy:sg',
    'copy:build:dev',
    'copy:build:dist',
    'htmlReplace:sg',
    'webpack:sg',
    'webpack:build',
    'uglify:sg',
    'uglify:build:dist',
    'zip:build'
  );
});
