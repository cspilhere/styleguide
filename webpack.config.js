var path = require('path');
var webpack = require('webpack');

var node_modules = path.resolve('./', 'node_modules');

module.exports = {
	output: {
		path: path.join(__dirname, '.tmp'),
    publicPath: '.tmp/',
		filename: 'scripts/bundle.js',
    pathinfo: true
	},
  devtool: 'eval',
  cache: true,
  watch: true,
  debug: true,
  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel?cacheDirectory'
      },
      {
				test: /\.html$/,
				loader: 'html-loader'
			},
      {
				test: /\.json$/,
				loader: 'json-loader'
			},
			{
				test: /\.css$/,
				loader: 'style-loader!css-loader'
			},
      {
        test: /\.scss$/,
        loaders: ['style', 'css', 'sass']
      }
    ]
	},
	resolve: {
    unsafeCache: true,
    extensions: ['', '.js', '.jsx', '.css'],
    alias: {
      dev: path.resolve(__dirname, './dev'),
			utils: path.resolve(__dirname, './dev/_app/core/utils'),
			components: path.resolve(__dirname, './dev/_app/core/components')
		}
	},
	plugins: [
    new webpack.optimize.DedupePlugin()
	]
};
