var webpack = require('webpack');
var path = require('path');

var config = {
  entry: './dev/_app/core/entry.js',
	output: {
		path: path.join(__dirname, './build'),
    publicPath: './build/',
		filename: 'scripts/bundle.js',
	},
  module: {
    noParse: [],
    loaders: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel'
      },
      {
				test: /\.html$/,
				loader: 'html-loader'
			},
      {
				test: /\.json$/,
				loader: 'json-loader'
			},
			{
				test: /\.css$/,
				loader: 'style-loader!css-loader'
			},
      {
        test: /\.scss$/,
        loaders: ['style', 'css', 'sass']
      }
    ]
	},
	resolve: {
    extensions: ['', '.js', '.jsx', '.css'],
    alias: {
      dev: path.resolve(__dirname, './dev'),
			utils: path.resolve(__dirname, './dev/_app/core/utils'),
			components: path.resolve(__dirname, './dev/_app/core/components')
		}
	},
	plugins: [
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.OccurenceOrderPlugin()
	]
};

module.exports = config;
