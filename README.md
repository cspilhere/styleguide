# 0800net

Para visualizar e editar o styleguide em ambiente de desenvolvimento é preciso instalar as dependências do projeto via NPM.
Para isso, basta baixar todos os arquivos do projeto em um repositório local e rodar o comando: "~ sudo npm install",
após a instalação basta rodar o comando: "gulp" que o projeto será compilado e aberto em seu navegador automaticamente.  

Requisitos: Node e Gulp
