let foreach = Array.prototype.forEach;

var accordion = function() {
  try {
    let accordionContainer = document.querySelector('.js-accordion');
    let accordionItems = accordionContainer.children;
    foreach.call(accordionItems, function(item) {
      if(item.nodeType === 1) {
        item.children[0].addEventListener('click', function() {
          item.classList.toggle('is-active');
        });
      }
    });
  } catch (e) {
    console.log('jsModule: accordion | ', e);
  }
};

export default accordion;
