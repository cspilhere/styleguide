let foreach = Array.prototype.forEach;

var dropdownCombo = function() {
  try {
    let dropdownCombo = document.querySelectorAll('.js-dropdown-combo');
    let dropdownComboTrigger = document.querySelectorAll('.js-dropdown-combo .dropdown-combo__trigger');
    let dropdownComboContainer = document.querySelectorAll('.js-dropdown-combo .dropdown-combo__container');
    let timer;

    foreach.call(dropdownCombo, function(el, index) {

      dropdownComboTrigger[index].addEventListener('click', function(e) {
        if (e.target.type === 'checkbox') {
          if (e.target.checked) {
            dropdownComboContainer[index].classList.add('is-open');
          } else {
            dropdownComboContainer[index].classList.remove('is-open');
          }
        } else {
          dropdownComboContainer[index].classList.toggle('is-open');
        }
      });

      document.addEventListener('click', (e) => {
        let dropdownOutside = e.path.filter((item) => {
          if(item.classList) {
            if(dropdownCombo[index] === item || dropdownComboTrigger[index] === item || dropdownComboContainer[index] === item) {
              return true;
            }
          }
        })[0];
        !dropdownOutside ? dropdownComboContainer[index].classList.remove('is-open') : null;
      });
    });


  } catch (e) {
    console.log('jsModule: dropdownCombo | ', e);
  }
};

export default dropdownCombo;
