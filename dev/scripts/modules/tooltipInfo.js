let foreach = Array.prototype.forEach;

var tooltipInfo = function() {
  try {
    let elementsWithTooltip = document.querySelectorAll('.js-tooltipInfo');
    let availableTooltips = document.querySelectorAll('.tooltip-info');

    let timer;

    foreach.call(elementsWithTooltip, function(item) {
      let tooltipId = item.getAttribute('data-tooltip-target');
      let tooltip = document.querySelector('#' + tooltipId);
      if(tooltip) {
        item.addEventListener('mouseover', function() {
          let itemConfig = item.getBoundingClientRect();
          let tooltipConfig = tooltip.getBoundingClientRect();
          timer = setTimeout(function() {
            tooltip.setAttribute('style',
              `left: ` +
                (
                  (tooltipConfig.width < itemConfig.left) ?
                    ((itemConfig.left - tooltipConfig.width) - 5) :
                    ((itemConfig.left + itemConfig.width) + 5)
                ) + `px;` +
              `top: ` + (itemConfig.top) + `px;` +
              `display: block; visibility: visible; opacity: 1;`
            );
          }, 100);
        });
        item.addEventListener('mouseout', function() {
          clearTimeout(timer);
          tooltip.setAttribute('style', 'top: -900px; left: -900px; visibility: hidden; opacity: 0;');
        });
      }
    });
  } catch (e) {
    console.log('jsModule: tooltipInfo | ', e);
  }
};
export default tooltipInfo;
