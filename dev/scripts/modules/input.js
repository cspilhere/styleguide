let foreach = Array.prototype.forEach;

var input = function() {
  try {

    // var foreach = Array.prototype.forEach;
    // var inputs = document.querySelectorAll('.wpcf7-tel');
    //
    // function mtel(v){
    //   v = v.replace(/\D/g, ''); //Remove tudo o que não é dígito
    //   v = v.replace(/^(\d{2})(\d)/g, '($1) $2'); //Coloca parênteses em volta dos dois primeiros dígitos
    //   v = v.replace(/(\d)(\d{4})$/, '$1-$2'); //Coloca hífen entre o quarto e o quinto dígitos
    //   return v;
    // }
    //
    // var regexPhone = /^(\([1-9]{2}\)\d{4}-\d{4,5})$/;
    // var regexReplace = /^\(*([1-9]{2})\)*|([0-9]{4})-*([0-9]{4,5})$/;

    let inputs = document.querySelectorAll('.input-text, .textarea');
    foreach.call(inputs, function(item) {
      item.addEventListener('keyup', function() {
        if(this.value !== "") {
          this.classList.add('is-filled');
        } else {
          this.classList.remove('is-filled');
        }
      });
    });
  } catch (e) {
    console.log('jsModule: input | ', e);
  }
};

export default input;
