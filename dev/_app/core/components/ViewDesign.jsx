import React from 'react';

import AppStore from '../stores/AppStore';

import __ from 'utils/localization';

import ViewHeader from './ViewHeader';
import ViewFooter from './ViewFooter';

import ColorPallete from './ColorPallete';
import FontWeight from './FontWeight';


export default class ViewDesign extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      content: {}
    };

    this._update = () => {
      this.setState({
        content: AppStore.getDesign(),
      });
    }
  }

  componentWillMount() {
    AppStore.addChangeListener(this._update);
  }

  componentWillUnmount() {
    AppStore.removeChangeListener(this._update);
  }

  componentDidMount() {
    this.setState({
      content: AppStore.getDesign(),
    });
  }

  render() {

    let titles = Object.keys(this.state.content);

    let abc =
    `A B C D E F G H I J K L M N O P Q R S T U V W X Y Z <br>
      a b c d e f g h i j k l m n o p q r s t u v w x y z <br>
      0 1 2 3 4 5 6 7 8 9`;

    let brand;
    let typography;
    let colors;

    if (Object.keys(this.state.content).length) {

      if (this.state.content.brand) {
        brand = this.state.content.brand.map((item, index) => {
          return (
            <div key={index} className="sgb-main__view-content">
              {!index ? <h1 className="sgb-heading sgb-heading--secondary">{__(titles[0])}</h1> : null}
              <h2 className="sgb-heading sgb-heading--primary">{item.title}</h2>
              {item.description ?
                <p className="sgb-body-text sgb-body-text--secondary">{item.description}</p>
                : null
              }
              <div
                className="sgb-brand"
                style={{backgroundColor: item.backgroundColor}}
              >
                <img src={item.url} alt="" />
              </div>
            </div>
          );
        });
      }

      if (this.state.content.typography) {
        typography = this.state.content.typography.map((item, index) => {
          return (
            <div key={index} className="sgb-main__view-content">
              {!index ? <h1 className="sgb-heading sgb-heading--secondary">{__(titles[1])}</h1> : null}
              <h2 className="sgb-heading sgb-heading--primary">{item.title}</h2>
              {item.description ?
                <p className="sgb-body-text sgb-body-text--secondary">{item.description}</p>
                : null
              }
              <p
              className="sgb-body-text sgb-body-text--secondary sgb-typo"
              style={{
                fontSize: '32px',
                fontFamily: item.family
              }} dangerouslySetInnerHTML={{__html: abc}}></p>
              <FontWeight weight={item.weight} family={item.family} />
            </div>
          );
        });
      }

      if (this.state.content.colors) {
        colors = this.state.content.colors.map((item, index) => {
          return (
            <div key={index} className="sgb-main__view-content">
              {!index ? <h1 className="sgb-heading sgb-heading--secondary">{__(titles[2])}</h1> : null}
              <h2 className="sgb-heading sgb-heading--primary">{item.title}</h2>
              {item.description ?
                <p className="sgb-body-text sgb-body-text--secondary">{item.description}</p>
                : null
              }
              <ColorPallete colors={item.colors} />
            </div>
          );
        });
      }

    }

    return (
      <div className="sgb-main">
        <ViewHeader title="Design" />
        <div className="sgb-main__view">
          {/*<div className="sgb-main__view-description">
            <p className="sgb-body-text sgb-body-text--primary">...</p>
          </div>*/}
          <div className="sgb-main__view-body">
            {brand}
            {typography}
            {colors}
          </div>
        </div>
        <ViewFooter />
      </div>
    );
  }
};
