import React from 'react';
import {Link} from 'react-router';

import classnames from 'classnames';

import AppStore from '../stores/AppStore';

import __ from 'utils/localization';
import {normalizeText} from 'utils/utils';

import SidebarChildNodes from './SidebarChildNodes';

let pkg = require('../../../../package.json');


export default class Sidebar extends React.Component {
  constructor() {
    super();

    this.state = {
      welcome: {}
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      welcome: nextProps.welcome
    });
  }

  render() {
    let nodes = Object.keys(this.props.nodes).map((item, index) => {
      let classes = classnames(
        'sgb-sidebar__nodes-header',
        {'is-link': !this.props.nodes[item].length},
        {'is-active': this.props.activePathName.match(item)}
      );

      return (
        <li key={index} className="sgb-sidebar__nodes-item">
          {!this.props.nodes[item].length ? <Link to={`/${item}`} className={classes}>{__(normalizeText(item))}</Link> : <div className={classes}>{__(normalizeText(item))}</div>}
          <SidebarChildNodes
            parent={item}
            childNodes={this.props.nodes[item]}
            activePathName={this.props.activePathName}
          />
        </li>
      );
    });
    return (
      <nav
        className="sgb-sidebar"
        role="navigation"
      >
        {this.state.welcome.logo ? <div className="sgb-sidebar__brand"><img src={this.state.welcome.logo} alt={pkg.name} /></div> : null}
        <ul className="sgb-sidebar__nodes">
          {nodes}
        </ul>
        <footer className="sgb-sidebar__footer"></footer>
      </nav>
    );
  }
};
