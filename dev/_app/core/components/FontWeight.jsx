import React from 'react';


export default class FontWeight extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    let weights = this.props.weight.map((item, index) => {
      return (
        <span
          key={index}
          className="sgb-body-text sgb-body-text--secondary"
          style={{
            fontSize: '48px',
            fontFamily: this.props.family,
            fontWeight: item,
            marginRight: '10px'
          }}
        >{item}</span>
      );
    });
    return (
      <div>
        Pesos<br/>
        {weights}
      </div>
    );
  }
};
