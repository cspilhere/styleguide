import React from 'react';
import {PropTypes} from 'react-router';

import ViewStore from '../stores/ViewStore';
import Actions from '../actions/Actions';

import ViewHeader from './ViewHeader';
import ViewFooter from './ViewFooter';

import {normalizeText, checkInvisibles} from 'utils/utils';

import Prism from 'utils/prism/prism';
import 'utils/prism/prism.css';


function nodePath(path) {
  if(path === 'components') {
    return '';
  } else {
    return path + '/';
  }
}


export default class ViewComponents extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      components: false,
      sectionData: {},
      currentSection: '...',
      activeModules: []
    };

    this._updateComponents = () => {
      this.setState({
        components: ViewStore.getComponents(),
        sectionData: ViewStore.getSectionInfo(),
        currentSection: ViewStore.componentSlug(),
        allModules: ViewStore.listJsModules()
      });
    };
  }

  componentWillMount() {
    ViewStore.addChangeListener(this._updateComponents);
  }

  componentWillUnmount() {
    ViewStore.removeChangeListener(this._updateComponents);
  }

  componentDidMount() {
    Actions.fetchComponents(nodePath(this.props.params.node) + this.props.params.section);
  }

  componentDidUpdate() {
    window.scrollTo(0, 0);

    if(!this.props.params.section) {
      this.context.history.pushState(null, '/components/' + ViewStore.componentSlug());
      Actions.fetchComponents(nodePath(this.props.params.node) + this.props.params.section);
    }

    this.state.allModules.forEach((item) => {
      typeof item.module === 'function' ? item.module() : item.module;
    });
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.params.section !== nextProps.params.section && nextProps.params.section) {
      Actions.fetchComponents(nodePath(nextProps.params.node) + nextProps.params.section);
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.components[0] !== this.state.components[0];
  }

  render() {
    let components = this.state.components.length > 0 ? this.state.components.map((item, index) => {

      return (
        <div key={index} className="sgb-main__view-content">
          <h2 className="sgb-heading sgb-heading--primary">{item.title}</h2>
          {item.description ?
            <p className="sgb-body-text sgb-body-text--secondary">{item.description}</p>
            : null
          }
          <div className="sgb-main__view-component" dangerouslySetInnerHTML={{__html: item.template}}></div>
          <pre
            className="language-markup sgb-main__view-code"
            style={{
              whiteSpace: 'pre-wrap'
            }}
            dangerouslySetInnerHTML={{__html: Prism.highlight(item.template, Prism.languages.markup) }}
          ></pre>
        </div>
      );
    }) : null;
    return (
      <div className="sgb-main">
        <ViewHeader title={normalizeText(this.state.currentSection)} />
        <div className="sgb-main__view">
          <div className="sgb-main__view-description">
            {this.state.sectionData.description ?
              <p className="sgb-body-text sgb-body-text--primary">{this.state.sectionData.description}</p>
              : null
            }
          </div>
          <div className="sgb-main__view-body">
            {components}
          </div>
        </div>
        <ViewFooter />
      </div>
    );
  }
};

ViewComponents.contextTypes = {history: PropTypes.history}
