import React from 'react';


export default class AppHeader extends React.Component {
  render() {
    return (
      <header className="sgb-main__header">
        <h1>{this.props.title}</h1>
      </header>
    );
  }
};
