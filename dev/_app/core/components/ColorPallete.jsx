import React from 'react';


export default class ViewDesign extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    let colorCard = this.props.colors.map((item, index) => {
      return (
        <div key={index} className="sgb-color__card">
          <div
            className="sgb-color__sample"
            style={{
              backgroundColor: item.hex
            }}
          ></div>
          <div className="sgb-color__hex">{item.hex}</div>
          <div className="sgb-color__description">{item.shortDescription}</div>
        </div>
      );
    });
    return (
      <div className="sgb-color-pallete">
        {colorCard}
      </div>
    );
  }
};
