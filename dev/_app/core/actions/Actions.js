import Dispatcher from '../dispatcher';
import Constants from '../constants';

let styleguideContent = require('dev/config.json');


let Actions = {
  fetchConfig: function(){
    Dispatcher.dispatch({
      type: Constants.FETCH_CONFIG,
      content: styleguideContent
    });
  },
  fetchComponents: function(section){
    Dispatcher.dispatch({
      type: Constants.FETCH_COMPONENTS,
      section: section
    });
  }
};

export default Actions
