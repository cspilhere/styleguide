import keyMirror from 'keymirror';

export default keyMirror({
  FETCH_CONFIG: null,
  FETCH_COMPONENTS: null,
  FETCH_DESIGN_CONTENT:  null
});
