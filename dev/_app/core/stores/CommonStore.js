const CHANGE_EVENT = 'change';

let CommonStore = {
  emitChange: function(message) {
    this.emit(CHANGE_EVENT, message);
  },
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }
};

export default CommonStore;
