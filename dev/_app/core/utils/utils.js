
function clean(content) {
  return content.match(/\.\/(?:(.*?))\/_data\.json/g) ?
    content.match(/\.\/(?:(.*?))\/_data\.json/g)[0]
    .replace(/\.\/(?:(.*?))\/_data\.json/g, '$1') : content;
}

export function normalizeText(text) {
  let normalized = text.match(/\.\/(?:(.*?))\/_data\.json/g) ?
    clean(text).match(/[a-zA-Z]+/g).join(' ') :
    (text.match(/[a-zA-Z]+/g) ? text.match(/[a-zA-Z]+/g).join(' ') : '');
  return normalized;
}

export function checkInvisibles(content) {
  content = content.replace('/\n/', 'as');
  console.log(content.match('/\n/') );
  return content;
}
