var userLang = navigator.language || navigator.userLanguage;

//'Entrada': ['PT', 'EN', 'ES']
var collection = {
  'welcome': ['início'],
  'components': ['componentes'],
  'brand': ['marca'],
  'typography': ['tipografia'],
  'colors': ['cores']
}

var __ = function(content) {
  return collection[content] ? collection[content][0] : content ;
}

module.exports = __;
